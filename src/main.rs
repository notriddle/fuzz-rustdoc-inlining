pub mod ast;

fn main() {
    let mut arg = std::env::args();
    let _ = arg.next().expect("usage: fuzz-rustdoc-inlining [seed-number]");
    let rnd = arg.next().unwrap_or_default();
    let mut rnd = rnd.parse().unwrap_or_else(|_| std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs());
    println!("// generated with fuzz-rustdoc-inlining {rnd}");
    let ast = ast::make_crate(&mut rnd);
    println!("#![allow(nonstandard_style)]");
    println!(r#"#![crate_name="foo"]"#);
    ast.print(Vec::new(), false, &ast);
}
