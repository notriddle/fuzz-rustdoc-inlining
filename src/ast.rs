pub struct Module {
    pub items: Vec<ModuleItem>,
}

impl Module {
    pub fn print(&self, path: Vec<Ident>, in_hidden: bool, krate: &Module) {
        for item in &self.items {
            item.print(path.clone(), in_hidden, krate);
        }
    }
    pub fn resolve_path<'a>(&'a self, path: &'_ [Ident]) -> (bool, &'a ModuleItem) {
        assert!(path.len() >= 1);
        for item in &self.items {
            if path[0] == item.name {
                if path.len() == 1 {
                    return (item.is_hidden, item);
                } else if let ModuleItemKind::Module { module } = &item.kind {
                    let (hidden, inner_item) = module.resolve_path(&path[1..]);
                    return (item.is_hidden || hidden, inner_item);
                } else {
                    panic!("cannot retrieve child of non-module");
                }
            }
        }
        panic!("path not found");
    }
}

pub enum ModuleItemKind {
    Module {
        module: Module,
    },
    ReExport {
        path: Vec<Ident>,
    },
    Struct,
}

impl ModuleItemKind {
    pub fn is_reexport(&self) -> bool {
        matches!(self, ModuleItemKind::ReExport { .. })
    }
}

pub struct ModuleItem {
    pub is_hidden: bool,
    pub kind: ModuleItemKind,
    pub name: Ident,
}

impl ModuleItem {
    pub fn print(&self, mut mod_path: Vec<Ident>, in_hidden: bool, krate: &Module) {
        let ident = self.name.0;
        let d = String::from_utf8(vec![b' '; mod_path.len() * 4]).expect("all spaces");
        if self.is_hidden {
            println!("{d}#[doc(hidden)]");
        }
        match &self.kind {
            ModuleItemKind::Struct => {
                if self.is_hidden || in_hidden {
                    println!(r#"{d}// @!has "foo/{mod_path}/struct.n{ident}.html""#, mod_path = path_string(&mod_path, "/"));
                } else {
                    println!(r#"{d}// @has "foo/{mod_path}/struct.n{ident}.html""#, mod_path = path_string(&mod_path, "/"));
                }
                println!("{d}pub struct n{ident};");
            }
            ModuleItemKind::Module { module } => {
                if self.is_hidden || in_hidden {
                    println!(r#"{d}// @!has "foo/{mod_path}/n{ident}/index.html""#, mod_path = path_string(&mod_path, "/"));
                } else {
                    println!(r#"{d}// @has "foo/{mod_path}/n{ident}/index.html""#, mod_path = path_string(&mod_path, "/"));
                }
                println!("{d}pub mod n{ident} {{");
                mod_path.push(self.name);
                module.print(mod_path, in_hidden || self.is_hidden, krate);
                println!("{d}}}");
            }
            ModuleItemKind::ReExport { path } => {
                let mut kit = self;
                let mut kit_hidden = false;
                while let ModuleItemKind::ReExport { path } = &kit.kind {
                    (kit_hidden, kit) = krate.resolve_path(path);
                }
                match &kit.kind {
                    ModuleItemKind::ReExport { .. } => unreachable!(),
                    ModuleItemKind::Module { .. } if !self.is_hidden && !in_hidden => {
                        if kit.is_hidden || kit_hidden {
                            // If hidden, inline
                            println!(r#"{d}// @has "foo/{mod_path}/n{ident}/index.html""#, mod_path = path_string(&mod_path, "/"));
                        } else {
                            // If not hidden, link
                            println!(r#"{d}// @has "foo/{mod_path}/index.html" "//a" "n{kit_ident}""#, mod_path = path_string(&mod_path, "/"), kit_ident = kit.name.0);
                            println!(r#"{d}// @!has "foo/{mod_path}/n{ident}/index.html""#, mod_path = path_string(&mod_path, "/"));
                        }
                    }
                    ModuleItemKind::Struct if !self.is_hidden && !in_hidden => {
                        if kit.is_hidden || kit_hidden {
                            // If hidden, inline
                            println!(r#"{d}// @has "foo/{mod_path}/struct.n{ident}.html""#, mod_path = path_string(&mod_path, "/"));
                        } else {
                            // If not hidden, link
                            println!(r#"{d}// @has "foo/{mod_path}/index.html" "//a" "n{kit_ident}""#, mod_path = path_string(&mod_path, "/"), kit_ident = kit.name.0);
                            println!(r#"{d}// @!has "foo/{mod_path}/struct.n{ident}.html""#, mod_path = path_string(&mod_path, "/"));
                        }
                    }
                    _ => {},
                }
                println!("{d}pub use crate::{path} as n{ident};", path = path_string(path, "::"));
            }
        }
    }
}

fn path_string(path: &[Ident], sep: &str) -> String {
    path.iter().map(|Ident(n)| format!("n{n}")).collect::<Vec<String>>().join(sep)
}

#[derive(Clone, Copy, PartialEq)]
pub struct Ident(pub u32);

pub fn step_rnd(rnd: &mut u64) -> u32 {
    // https://github.com/rust-lang/rust/blob/f7b831ac8a897273f78b9f47165cf8e54066ce4b/library/core/src/slice/sort.rs#L678-L693
    *rnd ^= *rnd << 13;
    *rnd ^= *rnd >> 7;
    *rnd ^= *rnd << 17;
    *rnd as u32
}

pub fn make_crate(rnd: &mut u64) -> Module {
    let mut collect = Vec::new();
    let mut module = make_module(rnd, &mut collect, Vec::new());
    fn fixup_reexports(items: &mut Vec<ModuleItem>, collect: &mut Vec<Vec<Ident>>, rnd: &mut u64, my_path: Vec<Ident>) {
        for item in items {
            let mut my_path = my_path.clone();
            my_path.push(item.name);
            match &mut item.kind {
                ModuleItemKind::Module { module } => {
                    fixup_reexports(&mut module.items, collect, rnd, my_path);
                }
                ModuleItemKind::ReExport { ref mut path } => {
                    if collect.len() == 0 {
                        item.kind = ModuleItemKind::Struct;
                    } else {
                        *path = collect[step_rnd(rnd) as usize % collect.len()].clone();
                        // push to collector now, after building it up, so that we can't have cycles
                        collect.push(my_path);
                    }
                }
                ModuleItemKind::Struct => {}
            }
        }
    }
    fixup_reexports(&mut module.items, &mut collect, rnd, Vec::new());
    module
}

pub fn make_module(rnd: &mut u64, collect: &mut Vec<Vec<Ident>>, path: Vec<Ident>) -> Module {
    let mut item_count = 0;
    let mut tries = 0;
    while item_count == 0 {
        item_count = (step_rnd(rnd) as usize) % (tries + 3);
        tries += 1;
    }
    let mut items = Vec::with_capacity(item_count);
    for _ in 0..item_count {
        let name = Ident(step_rnd(rnd));
        let mut path = path.clone();
        path.push(name);
        let kind = match step_rnd(rnd) % 3 {
            0 => {
                if path.len() >= 5 {
                    continue;
                }
                ModuleItemKind::Module {
                    module: make_module(rnd, collect, path.clone()),
                }
            },
            1 => ModuleItemKind::ReExport {
                path: Vec::new(), // will fill this in later
            },
            2 => ModuleItemKind::Struct,
            _ => unreachable!(),
        };
        if !kind.is_reexport() {
            // do not push exports until after the path is set,
            // to avoid cycles
            collect.push(path);
        }
        items.push(ModuleItem {
            is_hidden: (step_rnd(rnd) % 2) == 0,
            name,
            kind,
        });
    }
    Module {
        items
    }
}
